<?php

return array(
    'title' => 'Users',
    
    'single' => 'user',
    
    'model' => 'App\\User',
    
    'rules' => array(
        'email' => 'required|email',
        'username' => 'required',
    ),
    
    'messages' => array(
        'email.required' => 'the email field is required',
        'email.email' => 'the email field has no a valid format',
        'username.required' => 'the username field is required'
    ),
    
    'action_permissions'=> array(
        'create' => function()
        {
            return false;
        }
    ),
    
    'columns' => array(
        
        'id' => array(
            'title' => 'User Id'
        ),
        
        'name' => array(
            'title' => 'Fullname'
        ),
        
        'email' => array(
            'title' => 'Email'
        ),

        'formatted_banned' => array(
        	'title' => 'Banned'
    	)
    ),
    
    'filters' => array(
        'id',
        'name',
        'email',
        'banned' => array(
        	'title' => 'Banned',
        	'type' => 'bool'
    	)
    ),
    
    'edit_fields' => array(
        'email' => array(
            'title' => 'Email',
            'type' => 'text'
        ),
        'name' => array(
            'title' => 'Fullname',
            'type' => 'text'
        ),
        'banned' => array(
        	'title' => 'banned',
            'type' => 'bool'
    	)
    )
    
); 