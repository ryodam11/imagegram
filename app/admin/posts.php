<?php

return array(
    'title' => 'Posts',
    
    'single' => 'post',
    
    'model' => 'App\\Post',
    
    'rules' => array(
        
    ),
    
    'messages' => array(
        
    ),
    
    'action_permissions'=> array(
        'create' => function()
        {
            return false;
        }
    ),
    
    'columns' => array(
        
        'id' => array(
            'title' => 'Post Id'
        ),
        
        'description' => array(
            'title' => 'Description'
        ),

        'formatted_banned' => array(
        	'title' => 'Banned'
    	),

        'formatted_user_id' => array(
            'title' => 'User'
        )

    ),
    
    'filters' => array(
        'name',
        'banned' => array(
        	'title' => 'Banned',
        	'type' => 'bool'
    	)
    ),
    
    'edit_fields' => array(
        'banned' => array(
        	'title' => 'banned',
            'type' => 'bool'
    	)
    )
    
); 