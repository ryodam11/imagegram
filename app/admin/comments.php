<?php

return array(
    'title' => 'Comments',
    
    'single' => 'comment',
    
    'model' => 'App\\Comment',
    
    'rules' => array(
        
    ),
    
    'messages' => array(
        
    ),
    
    'action_permissions'=> array(
        'create' => function()
        {
            return false;
        },
        'update' => function(){
            return false;
        },
        'delete' => function(){
            return false;
        }
    ),
    
    'columns' => array(
        
        'id' => array(
            'title' => 'Comment Id'
        ),
        
        'comment' => array(
            'title' => 'Comment'
        ),
        
        'post_id' => array(
            'title' => 'Post Id'
        ),

        'formatted_user_name' => array(
        	'title' => 'User'
    	)
    ),
    
    'filters' => array(
        'name'
    ),
    
    'edit_fields' => array(
        'id' => array(
            'title' => 'Email',
            'type' => 'text'
        ),
        'comment' => array(
            'type' => 'textarea',
            'title' => 'Comment',
            'height' => 130
        ),
        'post_id' => array(
        	'title' => 'Post Id',
            'type' => 'text'
    	)
    )
    
); 