<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Post;

class LikeEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;
    public $like;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Post $post)
    {
        $this->like = ['likes' => $post->like, 'nolikes' => $post->nolike, 'post' => $post->id];
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['postaction'];
    }
}
