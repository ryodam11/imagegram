<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;
use App\Post;

class BannedUser extends Event implements ShouldBroadcast
{
    use SerializesModels;
    public $banned;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $bannedUser, Post $bannedPost)
    {
        $this->banned = ['user' => $bannedUser->id, 'post' => $bannedPost->id];
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['postaction'];
    }
}
