<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
	
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function comments(){
    	return $this->hasMany('App\Comment');
    }

    public function getFormattedBannedAttribute(){
        return ($this->getAttribute('banned') == 1)?'true':'false';
    }

    public function getFormattedUserIdAttribute(){
        $username = User::find($this->getAttribute('user_id'))->name;
        return $username;
    }

    public function hashtags(){
    	return $this->belongsToMany('App\Hashtag');
    }

}
