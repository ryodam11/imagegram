<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('auth/login', ['as' => 'login', 'uses' => 'HomeController@loginView']);

Route::post('auth/login', ['as' => 'post-login', 'uses' => 'HomeController@loginPost']);

Route::get('/sign-up', ['as' => 'get-sign-up', 'uses' => 'HomeController@signUpGet']);

Route::post('/sign-up', ['as' => 'post-sign-up', 'uses' => 'HomeController@signUpPost']);

Route::get('/logout', ['as' => 'logout', 'uses' => 'HomeController@logout']);

Route::get('/', ['middleware' => 'auth', 'as' => 'home', 'uses' => 'HomeController@homeView']);

Route::resource('post', 'PostController', [ 'only' => ['store', 'destroy', 'index', 'update', 'show'] ]);

Route::resource('comment', 'CommentController');