<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\SignupFormRequest;
use App\Http\Requests\SigninFormRequest;
use App\User;
use Auth;

class HomeController extends Controller
{

    public function homeView(){
        return \View::make('appViews.home');
    }

    public function loginPost(SigninFormRequest $request){
        $email = $request->get('email');
        $password = $request->get('password');
        if (Auth::attempt(['email' => $email, 'password' => $password, 'banned' => false])) {
            return redirect()->intended('/');
        }else{
        	return redirect()->route('login')->with('message', 'unregistered account or user banned');
        }
    }

	public function signUpGet(){
        return \View::make('appViews.signup');
    }

    public function signUpPost(SignupFormRequest $request){
    	$user = new User;
    	$user->name = $request->get('name');
    	$user->email = $request->get('email');
    	$user->password = \Hash::make($request->get('password'));
        $user->banned = false;
    	$email = $request->get('email');
    	if( $user->save() ){
    		\Mail::send('appViews.email',
	        [
	            'name' => $request->get('name'),
	            'password' => $request->get('password')
	        ], function($message) use ($email) {
		        $message->from('imagegramteam@gmail.com');
		        $message->to($email, 'Imagegram Team')->subject('Account Activated :)');
		    });

    		return \Redirect::route('get-sign-up')
      			->with("message", "We have sent an email, please check");
    	}else{
    		return \Redirect::route('get-sign-up')
      			->with("message", "A problem occurred, please try again.");
    	}
    }

    public function loginView(){
        return \View::make('appViews.login');
    }

    public function logout(){
        Auth::logout();
        return \Redirect::route('login');
    }

}
