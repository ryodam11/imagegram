<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use App\Post;
use App\User;
use Event;
use App\Events\BannedUser;

class PostController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::join('users', 'posts.user_id', '=', 'users.id')
                 ->where('posts.banned', '<>', true)
                 ->orderBy('created_at', 'desc')
                 ->get(
                        [
                            'users.name', 
                            'posts.description', 
                            'posts.image', 
                            'posts.created_at',
                            'posts.like',
                            'posts.nolike',
                            'posts.id'
                        ]
                    );
        $postsInfo = [];
        foreach ($posts as $post) {
            $comments =  Post::find($post->id)->comments()->join('users', 'comments.user_id', '=', 'users.id')->get(['comments.comment', 'users.name']);
            $postInfo = ['post' => $post, 'comments' => $comments];
            array_push($postsInfo, $postInfo);
        }
        return response()->json(
            [
                'posts' => $postsInfo
           ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        if( $request->hasFile('uploadfile') ){
            $postComment = $request->get('post-comment');
            $file = $request->file('uploadfile');
            $filedataurl = Image::make($file->getRealPath())->resize(null, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('data-url');
            $post = new Post;
            $post->image = $filedataurl;
            $post->description = $postComment;
            $post->like = 0;
            $post->nolike = 0;
            $post->user_id = \Auth::id();
            $post->banned = false;
            if( $post->save() ){
                return response()->json(['success' => true], 200);
            }
        }
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        /*$post =  Post::find($id)
                 ->join('users', 'posts.user_id', '=', 'users.id')
                 ->get(
                        [
                            'users.name', 
                            'posts.description', 
                            'posts.image', 
                            'posts.created_at',
                            'posts.like',
                            'posts.nolike',
                            'posts.id'
                        ]
                    )
                 ->first();*/
        $comments =  Post::find($id)->comments()->join('users', 'comments.user_id', '=', 'users.id')->get(['comments.comment', 'users.name']);
        $postInfo = ['post' => Post::find($id), 'comments' => $comments];
        return response()->json($postInfo, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if( $request->has('like') ){
            $post->like = ($post->like + 1);
            if( $post->save() ){
                return response()->json(['success' => true], 200);
            }else{
                return response()->json(['success' => false], 500);
            }
        }else if ($request->has('nolike')){
            $post->nolike = ($post->nolike + 1);
            if( $post->save() ){
                if( $post->nolike >= 30 ){
                   $bannedUser = User::find($post->user_id);
                   $bannedUser->banned = true;
                   $bannedPost = Post::find($post->id);
                   $bannedPost->banned = true;
                   if( $bannedUser->save() && $bannedPost->save() ){
                        Event::fire(new BannedUser($bannedUser, $bannedPost));
                   }
                }
                return response()->json(['success' => true], 200);
            }else{
                return response()->json(['success' => false], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
