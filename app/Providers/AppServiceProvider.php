<?php

namespace App\Providers;

use Event;
use App\Post;
use App\Comment;
use App\Events\PostCreated;
use App\Events\LikeEvent;
use App\Events\AddComment;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Post::created(function($post){
            Event::fire(new PostCreated($post));
        });
        Post::updated(function($post){
            Event::fire(new LikeEvent($post));
        });
        Comment::created(function($comment){
            Event::fire(new AddComment($comment));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
