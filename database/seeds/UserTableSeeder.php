<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Pedro Patron',
            'email' => 'pluispatron@gmail.com',
            'password' => \Hash::make('valeria20'),
            'banned' => false,
        ]);
        
        User::create([
            'name' => 'Usuario Prueba',
            'email' => 'email@dominio.com',
            'password' => \Hash::make('prueba123'),
            'banned' => false
        ]);
    }
}
