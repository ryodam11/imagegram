(function(){
	/**
	* imagegram.filters Module
	*
	* Description
	*/
	angular.module('imagegram.Filters', [])
	.filter('getPost', function(){
		return function(array, id){
			for( var i in array ){
				if( array[i].post.id === id){
					return array[i];
				}
			}
		}
	})
})();