(function(){
	angular.module('imagegram.Directives', [])
	.directive('mainHeader', [function(){
		return {
			restrict: 'E',
			templateUrl: 'js/app/partials/main-header.html'
		};
	}])
	.directive('mainFooter', [function(){
		return {
			restrict: 'E',
			templateUrl: 'js/app/partials/footer.html'
		};
	}])
	.directive('dropzone', ['csrf_token', 'app_root', function(csrf_token, app_root){
		return function(scope, element, attrs){
			var config = {
                url: app_root+'/post',
                maxFilesize: 100,
                paramName: 'imagefile',
                previewsContainer: '#image-previews',
                paramName: "uploadfile",
                maxThumbnailFilesize: 10,
                parallelUploads: 1,
                autoProcessQueue: false,
                addRemoveLinks: true,
                acceptedFiles: 'image/*',
                headers: { "X-CSRF-Token": csrf_token}
            };

            var eventHandlers = {

                'addedfile': function(file) {
                    scope.success = false;
                    scope.$apply();
                },

                'removedfile': function(file){

                },

                'success': function (file, response) {
                	scope.success = true;
                    scope.description = '';
                    scope.$apply();
                    dropzone.removeAllFiles();
                }
            };

			dropzone = new Dropzone(element[0], config);

			angular.forEach(eventHandlers, function(handler, event) {
                dropzone.on(event, handler);
            });

            scope.processDropzone = function() {
                dropzone.processQueue();
            };

		}
	}]);
})();