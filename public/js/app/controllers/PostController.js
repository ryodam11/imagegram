(function(){
	/**
	*  Module
	*
	* Description
	*/
	angular.module('imagegram.PostController', [])
	.controller('PostController', ['$scope', function($scope){

        $scope.filename = '';
        $scope.success = false;

        $scope.uploadFile = function() {
            $scope.processDropzone();
        };

	}]);
})();