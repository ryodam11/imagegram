(function(){
	/**
	*  Module
	*
	* Description
	*/
	var app = angular.module('imagegram', 
		[
			'doowb.angular-pusher',
			'ngRoute',
			'imagegram.HomeController',
			'imagegram.PostController',
			'imagegram.Directives',
			'imagegram.Services',
			'imagegram.Filters'
			
		]);

	app.config(['$httpProvider', 'csrf_token', function($httpProvider, csrf_token){
		$httpProvider.defaults.headers.common['X-CSRF-Token'] = csrf_token;
		$httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
		$httpProvider.defaults.headers.common["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
	}])

	app.config(['PusherServiceProvider', 'pusher_token',
	  function(PusherServiceProvider, pusher_token) {
	    PusherServiceProvider
	    .setToken(pusher_token)
	    .setOptions({});
	  }
	]);

	app.config(['$routeProvider', function($routeProvider){
		$routeProvider
		.when('/', {
			templateUrl: 'js/app/views/home.html',
			controller: 'HomeController'
		})
		.when('/send-post', {
			templateUrl: 'js/app/views/send-post.html',
			controller: 'PostController'
		})
		.otherwise({
			redirectTo: '/'
		});
	}]);
})();