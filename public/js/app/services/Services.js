(function(){
	/**
	*  Services
	*
	* Services consumed by controllers.
	*/
	angular.module('imagegram.Services', [])
	.service('PostServices', ['$http', '$q', function($http, $q){

		function getAllPosts(){
			var deferred = $q.defer();
			$http.get('post').then(
				function(posts){
					deferred.resolve(posts);
				},
				function(error){
					deferred.reject(error);
				}
			);
			return deferred.promise;
		}

		function getPost(postid){
			var deferred = $q.defer();
			$http.get('post/'+postid).then(
				function(posts){
					deferred.resolve(posts);
				},
				function(error){
					deferred.reject(error);
				}
			);
			return deferred.promise;
		}

		return {
			getAllPosts: getAllPosts,
			getPost: getPost
		}

	}])
	.service('CommentServices', ['$http','$q', function($http, $q){
		
		function addComment(postid, comment){
			var deferred = $q.defer();
			$http.post('comment', {postid: postid, comment: comment}).then(
				function(response){
					deferred.resolve(response);
				},
				function(error){
					deferred.reject(error);
				}
			)
			return deferred.promise;
		}

	return {
		addComment: addComment
	}

	}])
	.service('LikeServices', ['$http','$q', function($http, $q){
		function addLike(postid){
			var deferred = $q.defer();
			$http.put('post/'+postid, {like: 'like'}).then(
				function(response){
					deferred.resolve(response);
				},
				function(error){
					deferred.reject(error);
				}
			)
			return deferred.promise;
		}

		function addNoLike(postid){
			var deferred = $q.defer();
			$http.put('post/'+postid, {nolike: 'nolike'}).then(
				function(response){
					deferred.resolve(response);
				},
				function(error){
					deferred.reject(error);
				}
			)
			return deferred.promise;
		}

	return {
		addLike 	: addLike,
		addNoLike	: addNoLike
	}
		
	}])
})();