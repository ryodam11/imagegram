var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 elixir(function(mix) {

 	mix.styles([
		'../node_modules/bootstrap/dist/css/bootstrap-flat.min.css',
		'../node_modules/dropzone/dist/dropzone.css',
		'css/style.css'
 	], 'public/css/main.css', 'public');

 	mix.scripts([
 		'jquery/dist/jquery.min.js',
 		'bootstrap/dist/js/bootstrap.min.js',
 		'angular/angular.js',
 		'angular-route/angular-route.js',
 		'angular-pusher/angular-pusher.js',
 		'angular-animate/angular-animate.js',
 		'dropzone/dist/dropzone.js'
	], 'public/js/libs.js', 'node_modules');

	mix.version('public/css/main.css');

 });
