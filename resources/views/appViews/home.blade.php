<!DOCTYPE html>
<html lang="es">
<head>
	<meta name="viewport" content="width=device-width">
	<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{url().elixir('css/main.css')}}">
	<meta charset="UTF-8">
	<title>ImageGram</title>
</head>
<body ng-app="imagegram" ng-cloak>
	<header>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a href="#/" class="navbar-brand">ImageGram <i class="fa fa-camera-retro"></i></a>
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarcollapse" aria-expanded="false">
			        	<span class="sr-only">Toggle navigation</span>
			        	<span class="icon-bar"></span>
			        	<span class="icon-bar"></span>
			        	<span class="icon-bar"></span>
			      	</button>
				</div>
				<div class="collapse navbar-collapse" id="navbarcollapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#/">{{ App\User::find(Auth::id())->name }}</a></li>
						<li><a href="{{ URL::route('logout')}}">Cerrar Sesion</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<div ng-view></div>
	<main-footer></main-footer>
	<script src="{!! Asset('js/libs.js') !!}"></script>
	<script src="{!! Asset('js/app/app.js') !!}"></script>
	<script src="{!! Asset('js/app/controllers/HomeController.js') !!}"></script>
	<script src="{!! Asset('js/app/controllers/PostController.js') !!}"></script>
	<script src="{!! Asset('js/app/directives/Directives.js') !!}"></script>
	<script src="{!! Asset('js/app/services/Services.js') !!}"></script>
	<script src="{!! Asset('js/app/filters/Filters.js') !!}"></script>
	<script src="//js.pusher.com/2.2/pusher.min.js"></script>
	
	<script>
		/*var pusher = new Pusher();*/
	</script>
	<script type="text/javascript">
        Dropzone.autoDiscover = false;

      	angular.module('imagegram').constant('csrf_token', '{{ csrf_token() }}');
      	angular.module('imagegram').constant('pusher_token', "{{ env('PUSHER_KEY', '393fd82eee8af8405075') }}");
      	angular.module('imagegram').constant('app_root', "{{ url() }}");

      	// ( function( $, pusher, addPost ) {
 
		// var postActionChannel = pusher.subscribe( 'postaction' );
		 
		// postActionChannel.bind( "App\\Events\\PostCreated", function( data ) {
		// 	/*angular.*/
		// 	// addItem( data.id, false );
		// } );
		 
		// itemActionChannel.bind( "App\\Events\\ItemUpdated", function( data ) {
		 
		//     removeItem( data.id );
		//     addItem( data.id, data.isCompleted );
		// } );
		 
		// itemActionChannel.bind( "App\\Events\\ItemDeleted", function( data ) {
		 
		//     removeItem( data.id );
		// } );
		 
		// } )( jQuery, pusher, addPost );
    </script>

</body>
</html>