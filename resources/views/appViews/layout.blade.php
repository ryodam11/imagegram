<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width">
	<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{url().elixir('css/main.css')}}">
	<meta charset="UTF-8">
	<title>Imagegram Login</title>
</head>
<body>
	<header>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="row">

				</div>
				<div class="navbar-header">
					<a href="{{ URL::route('login') }}" class="navbar-brand">ImageGram <i class="fa fa-camera-retro"></i></a>
				</div>
			</div>
		</nav>
	</header>
	<section class="main">
		<div class="container">
			@yield('content')
		</div>
	</section>
	<script src="{!! Asset('js/libs.js') !!}"></script>
</body>
</html>