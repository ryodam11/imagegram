@extends('appViews.layout')
@section('content')
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="login-panel text-center">
				<form action="{{URL::route('post-sign-up')}}" method="post">
					{!! csrf_field() !!}
					<div class="form-group">
						<img src="image/user.png" class="img-responsive" alt="Login Image">
					</div>
					<div class="form-group">
						<div class="input-group margin-bottom-sm">
							<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
							<input class="form-control" name="name" type="text" placeholder="Name">
						</div>
						@if($errors->has('name'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
					</div>
					<div class="form-group">
						<div class="input-group margin-bottom-sm">
							<span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
							<input class="form-control" name="email" type="text" placeholder="Email">
						</div>
						@if($errors->has('email'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
							<input class="form-control" name="password" type="password" placeholder="Password">
						</div>
						@if($errors->has('password'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
							<input class="form-control" name="password-repeat" type="password" placeholder="Repeat Password">
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-primary">Singup <i class="fa fa-paper-plane-o"></i></button>
					</div>
				</form>
				@if( Session::has('message') )
					<div class="alert alert-info" role="alert">
	                    {{ Session::get('message') }}
	                </div>
				@endif
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<a href="{{URL::route('login')}}">Back to login</a>
		</div>
		<div class="col-md-2"></div>
	</div>
@stop